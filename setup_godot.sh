#!/bin/bash

BLUE='\033[0;34m'
NC='\033[0m' # No Color
COLOR="$BLUE"

source "${RIDERS_GLOBAL_PATH}/lib/helpers.sh"

# Godot Stuff
export GODOT_PYTHON_PORT=8000
ride_ports_expose "$GODOT_PYTHON_PORT" "$GODOT_PYTHON_PORT"
cd "${RIDE_SOURCE_PATH}/cezeri_godot_export"
python3 -m http.server &

echo -e "$COLOR"
echo Godot Web URI:
echo "$GODOT_WEB_URI"
echo -e "$NC"
